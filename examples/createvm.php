<?php
/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* coding style:
   In shell_exec, dont use variable expanision inside of quotes. Too easy to confuse with shell variable expansion.
 */

 /* these paths must match those used by the installed CreateVM scripts */
 /* directory to find port forwards in */
 $IPPORT_DIR = '/etc/ipmasq/createvm';

 /* directory the software is installed in */
 $VM_DIR = '/disk1/fai/createvm';

 /* synchronization directory path */
 $SYNC_DIR = '/var/local/createvm';

 /* the Security Realm string presented to the browser */
 $realm = 'Restricted Area';

 /* list of hosts admin may install */
 $hostnames = 'qemuoemrserver qemufaiserver qemugiftserver';

 /* list of cd images available */
 /* FIXME: just grab the directory contents. */
 $cd_images = 'fai_cd.iso fai_cd-stable-1.iso fai_cd-stable-2.iso';

 if (empty($_SERVER['PHP_AUTH_USER'])) {
  header('WWW-Authenticate: Basic realm="'.$realm.'"');
  header('HTTP/1.1 401 Unauthorized');
  die('Log in as Guest/Guest');
 }

 $auth_success=0;
 $auth_user='guest';
 $HTPASSWD_FILE=$VM_DIR.'/htpasswdfile';
 if (file_exists($HTPASSWD_FILE) && is_readable($HTPASSWD_FILE)){
  if($fp=fopen($HTPASSWD_FILE,'r')){
   $auth_success='False';
   while($line=fgets($fp)){
    /* first, remove line endings. */
    $line=preg_replace('`[\r\n]$`','',$line);
    list($fuser,$fpass)=explode(':',$line);
    if($fuser==$_SERVER['PHP_AUTH_USER']){
     /* assume DES encryption */
     /* the salt is the first 2 characters */
     $salt=substr($fpass,0,2);
     /* use the salt to encode the passed in password */
     $test_pw=crypt($_SERVER['PHP_AUTH_PW'],$salt);
     if($test_pw == $fpass){
      /* authentication succeeded */
      $auth_success='True';
      $auth_user=$fuser;
      fclose($fp);
      break;
     }
    }
   }
  }
 }
 if ($auth_success=='no') {
  fclose($fp);
 }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
<title>CreateVM Frontend V0.1</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<link rel="stylesheet" href="createvm.css" type="text/css"/>

<div class="welcome">Welcome to CreateVM.</div>

<div class="level_display">Access Level:<?php echo $auth_user; ?></div>

<div class="label">Currently Running VMs:</div>
<table class="vm list">
<form action="index.php" id="searchform" method="post"><?php

 /* this is to work around bugs in our shell script. */
 $RUNNING_VMS = shell_exec('cd '.$VM_DIR.' && ./bin/cm list | sort');

 foreach(explode(PHP_EOL, $RUNNING_VMS) as $line){
  if ($line != '' ) {
   list($VMUID, $VMSTATES) = explode(':', $line);
   echo '<tr class="vm description"><td class="vm uid">'.$VMUID.'</td><td class="vm state">'.$VMSTATES.'</td>';
   $VM_PORTS = shell_exec('cd '.$VM_DIR.' && ./bin/cm port '.$VMUID.' | grep -v address | sort');
   echo '<td class="port forward">';
   foreach(explode(PHP_EOL, $VM_PORTS) as $port){
    if ($port != '' ) {
     list($remainder, $inside_port)=explode(' to ',$port);
     list($trash, $outside_port)=explode(' from ',$remainder);
     switch ($inside_port) {
      case 443:
       echo '<a href="https://'.$_SERVER['HTTP_HOST'].':'.$outside_port.'/">https:'.$outside_port.'</a>';
       break;
      case 80:
       echo '<a href="http://'.$_SERVER['HTTP_HOST'].':'.$outside_port.'/">www:'.$outside_port.'</a>';
       break;
      case 22:
       echo '<a href="ssh://demo@'.$_SERVER['HTTP_HOST'].':'.$outside_port.'/">ssh:demo@'.$outside_port.'</a>';
       break;
      default:
       echo 'other:'.$outside_port;
      }
    }
   }
   echo '</td>'.PHP_EOL;
   echo '</tr>'.PHP_EOL;
   echo '<tr class="vm controls"><td>Note:<input type="text" name="'.$VMUID.'_note" value="';

   /* handle note fields */

   $NOTEFILE=$VM_DIR.'/'.$VMUID.'/note.txt';
   $NOTE_VALUE = '';

   /* if admin hit the submit button on this row */
   if ( ($auth_user == 'admin') && (isset($_POST[$VMUID.'_submit'])) ) {
    if ( ( file_exists($NOTEFILE) && is_writeable($NOTEFILE) ) || is_dir($VM_DIR.'/'.$VMUID) ) {
     /* if the file exists, overwrite it. */
     /* create file if file does not exist and the directory exists */
     $file_open=1;
     $fh = fopen($NOTEFILE, 'w') or $file_open=0;
     if ($file_open == 1) {
      fwrite($fh, $_POST[$VMUID.'_note']);
      fclose($fh);
     }
     else {
      echo "File Creation Error";
     }
    } else {
     echo "File Write Error";
    }
   }

   if ( file_exists($NOTEFILE) ) {
    $NOTE_VALUE = shell_exec('cat '.$NOTEFILE);
   }
   echo $NOTE_VALUE.'"\></td>'.PHP_EOL;
   echo '<td>Password:<input type="password" name="'.$VMUID.'_password"\></td>'.PHP_EOL;
   echo '<td><input type="submit" name="'.$VMUID.'_submit" value="Save Changes"\></td>'.PHP_EOL;
   echo '</tr>'.PHP_EOL;
  }
 }
?>
</table>
<table>
 <tr>
  <td>
<?php
  if ( ($auth_user == 'admin') && (isset($_POST['new_vm_create'])) ) {

  /* handle vm creation requests */

  /* grab a lockfile, different from the one createvm uses */
  $LOCKFILE_OUT = shell_exec('lockfile -r 1 '.$SYNC_DIR.'/'.'vm_request.lock');

  /* touch a file */

  /* unlock */
  $LOCKFILE_OUT = shell_exec('rm -f '.$SYNC_DIR.'/'.'vm_request.lock');
  
   }
 if ( ($auth_user == 'admin') || ($auth_user == 'guest') ) {

  ?><div class="new_vm_pane">
   <div class="label">New VM:</div>
   <select name="hostname">
<?php
  foreach (explode(' ',$hostnames) as $hostname) {
   echo '<option value="'.$hostname.'">'.$hostname.'</option>'.PHP_EOL;
  }
 ?>   </select> 

   <select name="cd_image">
<?php
  foreach (explode(' ',$cd_images) as $cd_image) {
   echo '<option value="'.$cd_image.'">'.$cd_image.'</option>'.PHP_EOL;
  }
 ?>   </select>
  </td>
  <td>New Password:<input type="password" name="requested_password"\></td>
<?php
 if ($auth_user == 'admin') {
 ?>  <td><input type="submit" name="new_vm_create" value="Create New Instance"\>
<?php
 }
 if ($auth_user == 'guest') {
 ?>  <td>Your Name:<input type="text" name="requestor_name"\></td>
  <td>Your Email Address:<input type="text" name="requestor_email_address"\></td>
  <td>Your Reasons for Requesting a VM:<input type="textarea" name="requestor_reasons"\></td>
  <td><input type="submit" name="new_vm_request" value="Request New Instance"\>
<?php
 }
 ?>
  </td>
 </div>
</tr>
<?php
 }
 ?>
</form>
</body>
