#!/bin/sh

# this is mostly empty, because for our purposes, the system should always be able
# to allocate another private range
ipblock_available() {
# check for networking support.
    if [ ! -d /proc/net/ ]; then
	{
	    echo "1";
	    return 1;
	}
    fi;
    echo "0"
    return 0;
}

# called by a pool script, to allocate a subnet.
# called with name of machine pool, and a UID for the subnet.
ipblock_allocate() {
    
    if [ ! -d $MGMT_DIR/ipblocks/ ]; then
# create a directory to claim ranges in.
	    mkdir $MGMT_DIR/ipblocks/
    fi;

    success=1;


# check to see what ranges are available.
    for loop in $(seq -s " " 16 31); do
	{
# make sure this range is not in use on an interface of the vm host.
	    if [ $(ipblock_check_available 172.$loop.0) = 0 ]; then
		{
# make sure theres no claim to it...
		    if [ ! -f $MGMT_DIR/ipblocks/*-*-172.$loop.0 ]; then
			{
# use it!
			    touch $MGMT_DIR/ipblocks/$1-$2-172.$loop.0
			    success=0;
			    break;
			}
		    fi
		}
	    fi
	}
    done;
    return $success;
}

# only called once all VMs using the pool are shut down.
# called with pool name, and UID of requested range.
ipblock_deallocate() {
# check to make sure our glob only matches one entry.
    TARGETS=$(find $MGMT_DIR/ipblocks/$1-$2-172.* | wc -l)
    if [ $TARGETS = 1 ]; then
	{
	    rm -f $MGMT_DIR/ipblocks/$1-$2-172.*
	    return 0;
	}
    else
	return 1;
    fi
}

# make sure the range in question is not being used on an interface on the vm host.
ipblock_check_available() {
    IPBLOCK_ALREADY_SETUP=$(ip addr show to $1/255.255.255.0)
    if [ -z "$IPBLOCK_ALREADY_SETUP" ]; then
	{
	    echo "0";
	    return 0;
	}
    fi
    echo "1";
    return 0;
}

#given the unique key used to allocate a block, print out the subnet assigned
# passed the pool, and the UID.
ipblock_find() {
# check to make sure our glob only matches one entry.
    TARGETS=$(find $MGMT_DIR/ipblocks/ -name $1-$2-*| wc -l);
    if [ $TARGETS = 1 ]; then
	{
	    echo $(ls $MGMT_DIR/ipblocks/$1-$2-*| sed "s|$MGMT_DIR/ipblocks/$1-$2-||");
	    return 0;
	}
    elif [ $TARGETS -gt 1 ]; then
	    return 1;
    fi
#if no range was found
    return 0;
}
