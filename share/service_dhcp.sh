# write out a dhcp configuration fragment for the ip block allocated.
# arguments:
# $1 = the pool to allocate out of
# $2 = the unique id of the ip block
# $3 = how many machines to allow in the block
# $4 = an optional flag indicating whether we need to account for port forwarding or not.
dhcp_allocate() {
    IPBLOCK=$(ipblock_find $1 $2);
    if [ ! -d $MGMT_DIR/isc-dhcp-server ] ; then
	mkdir -p $MGMT_DIR/isc-dhcp-server
    fi
    LASTIP=$((2+$3))
    cat > $MGMT_DIR/isc-dhcp-server/$1.conf <<EOF
subnet $IPBLOCK.0 netmask 255.255.255.0 {
  range $IPBLOCK.2 $IPBLOCK.$LASTIP;
  option routers $IPBLOCK.1;
  option domain-name-servers $IPBLOCK.1;
EOF
    if [ ! -z "$4" ] ; then
	{
	    cat >> $MGMT_DIR/isc-dhcp-server/$1.conf <<EOF
  on commit {
    set ClientIP = binary-to-ascii(10, 8, ".", leased-address);
    set ClientMac = ucase(binary-to-ascii(16, 8, ":", substring(hardware, 1, 6)));
    log(concat("Commit: IP: ", ClientIP, " Mac: ", ClientMac));
    execute("/etc/dhcp/dhcpd-createvm.sh", "Commit", "$1", "$PREFIX" , ClientIP, ClientMac);
  }
}
EOF
	}
	else
	{
	    cat >> $MGMT_DIR/isc-dhcp-server/$1.conf <<EOF
}
EOF
	}
    fi
    dhcp_regen_conf_fragment
}

# destroy our configuration fragment.
dhcp_deallocate() {
    if [ -f $MGMT_DIR/isc-dhcp-server/$1.conf ]; then
	rm $MGMT_DIR/isc-dhcp-server/$1.conf
    fi
    dhcp_regen_conf_fragment
    return 0;
}

dhcp_regen_conf_fragment() {
# trunicate our target file
    cp /dev/null $DHCP_DIR/dhcpd.conf.autogen

    for each in $(find $MGMT_DIR/isc-dhcp-server/ -type f); do
	echo include \"$each\"";" >> $DHCP_DIR/dhcpd.conf.autogen
    done;
    return 0;
}
