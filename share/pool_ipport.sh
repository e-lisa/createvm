#!/bin/sh

# check and make sure we can reserve an outside port, and that one is available.
# ARGUMENTS:
# $1 == lowest port in range we want on the outside interface (internet accessable)
# $2 == high port
ipport_available() {
    if [ ! -d /sys/module/ip_tables/ ]; then
	{
	    echo "1";
	    return 1;
	}
    fi;

    success=1;
    for port in $( seq -s " " $1 $2); do
	{
# make sure the port in question is not in use.
	    PORT_AVAILABLE=$(ipport_check_available $port)
	    if [ "$PORT_AVAILABLE" = "0" ]; then
		{
# make sure theres no claim to it...
		    if [ ! -d $IPPORT_DIR ]; then
			{
			    success=0;
			    break;
			}
		    else
			{
			    if [ ! -f $IPPORT_DIR/*-$port ]; then
				{
				    success=0;
				    break;
				}
			    fi
			}
		    fi
		}
	    fi
	}
    done;
    echo $success;
    return $success;
}

# called by a pool script, to allocate a port forward.
# ARGUMENTS:
# $1 == UID of pool
# $2 == hostname of machine being forwarded to
# $3 == UID of host inside of pool+hostname
# $4 == UID of port forward, unique in this pool
# $5 == inside port number
# $6 == lowest port in range we want on the outside interface (internet accessable)
# $7 == high port
# called with UID of pool, a UID of a port forward within that pool,
# target inside port, beginning of range for outside port, and end of range for outside port.
ipport_allocate() {
    
# check to see if we've claimed any ports.
    if [ ! -d $IPPORT_DIR ]; then
	{
# if not, create a directory to claim then in.
	    mkdir $IPPORT_DIR
	}
    fi;

    success=1;
    for port in $( seq -s " " $6 $7); do
	{
# make sure the port in question is not in use.
	    PORT_AVAILABLE=$(ipport_check_available $port)
	    if [ "$PORT_AVAILABLE" = "0" ]; then
		{
# make sure theres no claim to it...
# check to see if we've claimed any ports.
		    if [ ! -f $IPPORT_DIR/*-$port ]; then
			{
# use it!
			    touch $IPPORT_DIR/$1-$2-$3-$4-$5-$port
			    success=0;
			    break;
			}
		    fi
		}
	    fi
	}
    done;
    echo $success;
    return $success;
}

# called during VM destruction
# arguments: UID of pool, UID of forward
ipport_deallocate() {
    IPPORT_ALLOCATED=$(find $IPPORT_DIR/ -name "${1}-*-*-${2}-*-*")
    if [ ! -z "$IPPORT_ALLOCATED" ]; then
	{
	    for each in $IPPORT_ALLOCATED; do
		{
		    VM_HOSTNAME_UID=$(echo $each| sed -n "s|$IPPORT_DIR/${1}-\(.*\)-${2}.*|\1|p")
		    rm -f $IPPORT_DIR/$(echo $each | sed -n "s|$IPPORT_DIR/||p")
		    rm -f "$each"
		}
	    done;
	    # remove arno's memory of the ip.
	    rm -f $ARNO_DIR/${1}-$VM_HOSTNAME_UID-[0-9.]*
	    return 0;
	}
    else
	{
	    echo "1";
	    return 1;
	}
    fi
}

# make sure the port has yet to be configured, AKA, nothing is listening on it. otherwise fail.
# FIXME: assumes you have just one outbound interface.
# argument: port number being checked.
ipport_check_available() {
    OUTBOUNDDEV=$(ip route show | sed -n 's/default via .* dev \([a-z0-9A-Z]*\).*/\1/p')
    OUTBOUNDIP=$(ip addr | sed -n 's/.*inet \(.*\)\/.* brd .* '${OUTBOUNDDEV}'/\1/p')

    IPPORT_LISTENING_OUTSIDE=$(netstat -lnt4| grep "$OUTBOUNDIP:$1")
    IPPORT_LISTENING_ALL=$(netstat -lnt4| grep "0.0.0.0:$1")
    if [ -z "$IPPORT_LISTENING_OUTSIDE" -a -z "$IPPORT_LISTENING_ALL" ]; then
	{
	    echo "0";
	    return 0;
	}
    fi
    echo "1";
    return 1;
}

# given the unique key used to allocate a port forward within a pool, print out the outside port number
ipport_find() {
    TARGETS=$(find $IPPORT_DIR/ -name "$1-[A-Za-z]*-[0-9]*-$2-*" | wc -l);
    if [ $TARGETS = 1 ]; then
	{
	    echo $(find $IPPORT_DIR/ -name "$1-[A-Za-z]*-[0-9]*-$2-*"| sed -n "s|$IPPORT_DIR/.*-[0-9]*-\([0-9]*\)|\1|p")
	    return 0;
	}
    fi
    return 0;
}
