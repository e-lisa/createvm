#!/bin/sh
/sbin/ifconfig $1 0.0.0.0 promisc down

IP="/sbin/ip"
IFCONFIG="/sbin/ifconfig"

. ./ifup-bridge-variables.sh

# remove ourself from the bridge.
$BRCTL delif $BRIDGE $1


BRIDGEDEV=`$BRCTL show|grep -E ^"$BRIDGE" | grep tap`

if [ -z "$BRIDGEDEV" ] ; then
    {
# we are the last one out. destroy the bridge.
	$IFCONFIG $BRIDGE down
        $BRCTL delif $BRIDGE $1
        $BRCTL delbr $BRIDGE
    }
fi
