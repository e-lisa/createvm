LVDISPLAY=/sbin/lvdisplay

output=$($LVDISPLAY -C | grep '\-wi-a---' | grep tearoff | sed "s/\(.*\).tearoff.*/\1\n/")

echo $output
