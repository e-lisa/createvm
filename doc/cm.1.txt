CM(1)
=====
:doctype: manpage

NAME
----
cm - Utility to get information about virtual machines created by createvm(1M)

SYNOPSIS
--------
*cm* ('OPTIONS' ...)

DESCRIPTION
-----------
The cm(1) command allows you to see information about virtual machines created by createvm(1M).  You can list the virtual machines, add notes about the virtual machines, get a list of port forwards assigned to virtual machines and destroy existing virtual machines.

OPTIONS
-------
*list, list* 'vm'::
    List the existing virtual machines.
*kill* 'vm'::
    Delete/Destroy and existing virtual machine and reap all its resources.
*ports, ports* 'vm'::
    Display the port forwards from the host to the virtual machine(s).
*services, services* 'search'::
    Display a list of services and their domain/virtual machine pairing.
*setservice, setservice* 'vm' 'domain' 'service'::
    Sets a service to be handled for a domain by a virtual machine. Services accepted: http.
*rmservice, rmservice* 'vm' 'domain' 'service'::
    Removes a service from a virtual machine.
*forwards, forwards* 'search'::
    Display a list of forwards for which domains to what IPs.
*setforward, setforward* 'IP' 'domain' 'service'::
    Sets a domain to be handled by another machine, by IP. Services accepted: http.
*rmforward, rmforward* 'IP' 'domain' 'service'::
    Removes a forward.
*notes, notes* 'vm'::
    Display the notes associated with any virtual machine.
*setnote, setnote* 'vm' 'note'::
    Sets a note to a virtual machine.

EXAMPLES
--------
*List virtual machines and their status*::
    'cm list'
*Display the port forwards for each virtual machine*::
    'cm ports'
*Display the notes stored about a virtual machine*::
    'cm notes'
*Delete a virtual machine in the tearoff poll named qemufaiserver*::
    'cm kill qemufaiserver-tearoff-6'
*Display usage information*::
    'cm'

SEE ALSO
--------
createvm(1M)

REPORTING BUGS
--------------
Please report bugs via email.  Patches welcome.  Having a published git tree with your patches makes it easy to integrate your patches.

COPYRIGHT
---------
(c) 2011 Zac Slade <krakrjak@gmail.com>

(c) 2011 Julia Longtin <julia.longtin@gmail.com>

(c) 2012 Lisa Marie Maginnis <lisam@faikvm.com>
